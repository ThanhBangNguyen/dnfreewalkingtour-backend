package asia.ncc.estimation.tool.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

    @Repository
    @Transactional
    public class TestRepository {
        @Autowired
        JdbcTemplate jdbcTemplate;
        public int deleteWBSByIdWORK_ITEM(long id) {
            return jdbcTemplate.update("delete from WBS where WORK_ITEM_IDS_ID=?", new Object[] { id });
        }
        public int deleteWBSByIdPROJECTS(long id) {
            return jdbcTemplate.update("delete from WBS where PROJECTS_ID=?", new Object[] { id });
        }

        public int deleteAssumptiopnByIdWorkItem(long id) {
            return jdbcTemplate.update("delete from ASSUMPTION where WORK_ITEM_ID=?", new Object[] { id });
        }

        public int deleteCustomerByIdQuotation(long id){
            return jdbcTemplate.update("delete from CUSTOMER where QUOTATION_ID=?", new Object[] { id });
        }
    }

