package asia.ncc.estimation.tool.service.dto;

public class SendEmailDTO {
    private  Long CustomerId ;
    private String subbject;
    private  String text;
    private  String attachment;
    private  String fileName;
    private  String  to ;

    public Long getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(Long customerId) {
        CustomerId = customerId;
    }

    public String getSubbject() {
        return subbject;
    }

    public void setSubbject(String subbject) {
        this.subbject = subbject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
