package asia.ncc.estimation.tool.service.dto;

public class WordItemDTO {
    private int no;
    private String TaskName;
    private String codingErffort;
    private String indayte;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String taskName) {
        TaskName = taskName;
    }

    public String getCodingErffort() {
        return codingErffort;
    }

    public void setCodingErffort(String codingErffort) {
        this.codingErffort = codingErffort;
    }

    public String getIndayte() {
        return indayte;
    }

    public void setIndayte(String indayte) {
        this.indayte = indayte;
    }
}
