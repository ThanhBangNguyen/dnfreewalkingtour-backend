package asia.ncc.estimation.tool.web.rest;

import asia.ncc.estimation.tool.domain.Customer;
import asia.ncc.estimation.tool.domain.Project;
import asia.ncc.estimation.tool.repository.CustomerRepository;
import asia.ncc.estimation.tool.repository.TestRepository;
import asia.ncc.estimation.tool.service.MailService;
import asia.ncc.estimation.tool.service.dto.CreateQuotationDTO;
import asia.ncc.estimation.tool.service.dto.Reponse.Response;
import asia.ncc.estimation.tool.service.dto.SendEmailDTO;
import com.codahale.metrics.annotation.Timed;
import asia.ncc.estimation.tool.domain.Quotation;
import asia.ncc.estimation.tool.repository.QuotationRepository;
import asia.ncc.estimation.tool.web.rest.errors.BadRequestAlertException;
import asia.ncc.estimation.tool.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Quotation.
 */
@RestController
@RequestMapping("/api")
public class QuotationResource {

    private final Logger log = LoggerFactory.getLogger(QuotationResource.class);

    private static final String ENTITY_NAME = "quotation";

    private final QuotationRepository quotationRepository;
    @Autowired
    TestRepository testRepository ;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private MailService mailService;

    public QuotationResource(QuotationRepository quotationRepository) {
        this.quotationRepository = quotationRepository;
    }

    /**
     * POST  /quotations : Create a new quotation.
     *
     * @param quotation the quotation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new quotation, or with status 400 (Bad Request) if the quotation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/quotations")
    @Timed
    public ResponseEntity<Quotation> createQuotation(@RequestBody Quotation quotation) throws URISyntaxException {
        log.debug("REST request to save Quotation : {}", quotation);
        if (quotation.getId() != null) {
            throw new BadRequestAlertException("A new quotation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Quotation result = quotationRepository.save(quotation);
        return ResponseEntity.created(new URI("/api/quotations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /quotations : Updates an existing quotation.
     *
     * @param quotation the quotation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated quotation,
     * or with status 400 (Bad Request) if the quotation is not valid,
     * or with status 500 (Internal Server Error) if the quotation couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/quotations")
    @Timed
    public ResponseEntity<Quotation> updateQuotation(@RequestBody Quotation quotation) throws URISyntaxException {
        log.debug("REST request to update Quotation : {}", quotation);
        if (quotation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Quotation result = quotationRepository.save(quotation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, quotation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /quotations : get all the quotations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of quotations in body
     */
    @GetMapping("/quotations")
    @Timed
    public List<Quotation> getAllQuotations() {
        log.debug("REST request to get all Quotations");
        return quotationRepository.findAll();
    }

    /**
     * GET  /quotations/:id : get the "id" quotation.
     *
     * @param id the id of the quotation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the quotation, or with status 404 (Not Found)
     */
    @GetMapping("/quotations/{id}")
    @Timed
    public ResponseEntity<Quotation> getQuotation(@PathVariable Long id) {
        log.debug("REST request to get Quotation : {}", id);
        Optional<Quotation> quotation = quotationRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(quotation);
    }

    /**
     * DELETE  /quotations/:id : delete the "id" quotation.
     *
     * @param id the id of the quotation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/quotations/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuotation(@PathVariable Long id) {
        log.debug("REST request to delete Quotation : {}", id);

        quotationRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/getQuotationByProjects/{id}")
    public ResponseEntity<Quotation> getQuotationByProjects(@PathVariable Long id) {
        List<Quotation> projectbyQuotaion = quotationRepository.findAll();
        for (Quotation quotation : projectbyQuotaion
        ) {
            if (quotation.getProjectID().getId() == id) {
                Optional<Quotation> st = Optional.of(quotation);
                return ResponseUtil.wrapOrNotFound(st);
            }
        }
        return null;
    }

    @GetMapping("/getQuotationByCustomer/{id}")
    public ResponseEntity<Quotation> getQuotationByCustomer(@PathVariable Long id) {
        List<Quotation> projectbyQuotaion = quotationRepository.findAll();
        for (Quotation quotation : projectbyQuotaion
        ) {
            if (quotation.getCustomerId().getId() == id) {
                Optional<Quotation> st = Optional.of(quotation);
                return ResponseUtil.wrapOrNotFound(st);
            }
        }
        return null;
    }

    @PutMapping("/delCustomer/{id}")
    public void deleteQuotationByCustomer(@PathVariable Long id) {
        log.debug("REST request to delete Quotation by Customer: {}", id);
        List<Quotation> list = quotationRepository.findAll();
        List<Customer> listC = customerRepository.findAll();
        Customer customer= new Customer();
        for (Quotation quotation : list) {
            if (quotation==null) break;
            if (quotation.getCustomerId().getId() == id) {
                quotationRepository.delete(quotation);
                customerRepository.delete(customer);
            }
        }
        for (Customer customer1: listC){
            if (customer1.getId()==id){
                customer=customer1;
                customerRepository.delete(customer);
            }
        }
    }

    @DeleteMapping("/delQuotationsById/{id}")
    @Timed
    public ResponseEntity<Void> deleteQuotationbyId(@PathVariable Long id) {
        log.debug("REST request to delete Quotation : {}", id);
        quotationRepository.deleteById(id);
        testRepository.deleteCustomerByIdQuotation(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    @PostMapping("/sendTocustomer")
    public Response<SendEmailDTO> createQuotation(@RequestBody SendEmailDTO sendEmailDTO){
        sendEmailDTO = mailService.sendMaiToCusTomer(sendEmailDTO);
        return new Response<>(true, sendEmailDTO, "Sent message successfully....!");
    }



}
